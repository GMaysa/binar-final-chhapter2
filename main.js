// 1
// Buatlah sebuah function dengan nama changeWord yang berfungsi untuk menggantikan sebuah kata didalam sebuah kalimat.

    // function changeWord(selectedText, changedText, text) {
    //     return text.replace(selectedText, changedText)
    //     // function replace will replace the selectedText in the text with changedText
    // }

    // console.log(changeWord("Text", "String", "Text in action"))

// ============================== END OF NO 1 ============================== //

// 2.
// Buatlah sebuah function yang berfungsi mendeteksi apakah sebuah angka termasuk angka genap atau ganjil.

    // const checkTypeNumber = (givenNumber) =>{
    //     if(givenNumber == null){
    //         // if the parameter is null the function will return this string
    //         return "Error: Bro where is the parameter?"
    //     }else if(typeof(givenNumber) != "number"){
    //         // if the data type of givenNumber is not a number the function will return this string
    //         return "Error: Invalid data type"
    //     }else if(givenNumber % 2 == 0){
    //         // if the givenNumber is even, the function will return this string
    //         return "GENAP";
    //     }else {
    //         // end at the and of conditioning, if all the condition is false, this condition will return this string
    //         return "GANJIL";
    //     }
    // }

    // console.log(checkTypeNumber())

// ============================== END OF NO 2 ============================== //

// 3
// Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah alamat email yang diberikan sebagai parameter, 
// adalah alamat email yang formatnya benar atau tidak.

    // function checkEmail(email) {
    //     let matches = /\S+@+\S+\./
    //     if(matches.exec(email)){
    //         console.log("VALID")
    //     }else if(/@/.exec(email)){
    //         console.log("INVALID");
    //     }else{
    //         console.log("ERROR : ")
    //     }
    // }

    // checkEmail()

// ============================== END OF NO 3 ============================== //

// 4
// Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah password yang diberikan sebagai parameter memenuhi kriteria
// yang telah ditentukan atau tidak.

    // function isValidPassword(pss) {
    //     if(pss.length >= 8 && /[a-z]/g.exec(pss) != null && /[A-Z]/g.exec(pss) != null && /\d/g.exec(pss) != null){
    //         return true
    //     }
    //     else{
    //         return false
    //     }
    // }

    // console.log(isValidPassword("Ssssss9"))

// ============================== END OF NO 4 ============================== //

// 5
// Buatlah sebuah function yang berfungsi untuk membagikan sebuah nama menjadi Nama Depan, Nama Tengah, Nama Belakang.
// Function ini nantinya akan menerima satu parameter yang berisi nama lengkap seseorang. Apabila nama lengkap dari seseorang tersebut
// lebih dari 3 suku kata, maka function tersebut harus menghasilkan sebuah error. Tapi apabila parameter yang diberikan valid (tidak lebih dari
// 3 suku kata), maka function ini akan menghasilkan sebuah object dengan properti firstName, middleName, lastName.

    // const getSplitName = (personName) => {
    //     if(personName == undefined){
    //         // jika parameter kosong atau tidak terdapat value didalamnya maka fungsi akan mengembalikan string di Bawah
    //         return 'ERROR: Nama tidak dapat ditemukan!'
    //     } else if(typeof personName != 'string'){
    //         // jika parameter bukanlah string didalamnya maka fungsi akan mengembalikan string di Bawah
    //         return 'ERROR: Nama haruslah string!'
    //     }

    //     // trim digunakan untuk menghapus spasi jika terdapat spasi diawal atau diakhir kalimat / string
    //     // split digunakan untuk memisahkan kata dari kalimat lalu menjadikannya array substring lalu mengembalikan array baru tanpa mengubah string aslinya
    //     const nameParts = personName.trim().split(' ');

    //     if (nameParts.length > 3) {
    //         return 'Nama lengkap tidak boleh lebih dari 3 suku kata.'
    //     }

    //     const [firstName, ...middleName] = nameParts;
    //     // console.log(middleName)
    //     const lastName = middleName.pop() || '';
    //     return {
    //         firstName: firstName || '',
    //         middleName: middleName.join('') || '',
    //         lastName: lastName || ''
    //     };
    // }
    
    // console.log(getSplitName("Aldi Daniella Guna Pranata"))
    // console.log(getSplitName("Aldi Daniella Pranata"))
    // console.log(getSplitName("Aldi Pranata"))
    // console.log(getSplitName("Aldi"))
    // console.log(getSplitName(0))
    // console.log(getSplitName())

// ============================== END OF NO 5 ============================== //

// 6
// Buatlah sebuah function yang berfungsi untuk mendapatkan angka terbesar kedua dari sebuah array.
// Misal diberikan sebuah array yang terdiri dari beberapa angka [2,3,5,6,6,4], berdasarkan data dari array tersebut dapat kita simpulkan
// bahwasanya angka terbesar dari array tersebut adalah 6, angka kedua terbesar adalah 5, dan angka ketiga terbesar adalah 4. Maka dari itu
// function yang akan kamu buat ini akan me-return angka kedua terbesar pada array yang telah diberikan, yaitu angka 5.

    // function getAngkaTerbesarKedua(arr) {
    //     if(arr == undefined) {
    //         return 'ERROR: array tidak boleh kosong'
    //     } else if(!Array.isArray(arr)) {
    //         return 'ERROR: anda harus memasukkan value kedalam array'
    //     }

    //     let max = -Infinity;
    //     let secondMax = -Infinity;
    
    //     for (let i = 0; i < arr.length; i++) {
    //         const currentNumber = arr[i];
    
    //         if (currentNumber > max) {
    //             secondMax = max;
    //             max = currentNumber;
    //         } else if (currentNumber > secondMax && currentNumber !== max) {
    //             secondMax = currentNumber;
    //       }
    //     }
    
    //     if (secondMax === -Infinity) {
    //         throw new Error('Array harus memiliki setidaknya 2 angka yang berbeda.');
    //     }
    
    //     return secondMax;
    // }
    
    // const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8]

    // console.log(getAngkaTerbesarKedua(0))

// ============================== END OF NO 6 ============================== //

// 7
// Tugas kamu adalah membuat sebuah function yang berfungsi
// membantu Pak Aldi untuk menghitung total seluruh sepatu yang
// terjual.

// const dataPenjualanPakAldi = [
//     {
//       namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
//       hargaSatuan: 760000,
//       kategori : "Sepatu Sport",
//       totalTerjual : 90,
//     },
//     {
//       namaProduct : 'Sepatu Warrior Tristan Black Brown High',
//       hargaSatuan: 960000,
//       kategori : "Sepatu Sneaker",
//       totalTerjual : 37,
//     },
//     {
//       namaProduct : 'Sepatu Warrior Tristan Maroon High ',
//       kategori : "Sepatu Sneaker",
//       hargaSatuan: 360000,
//       totalTerjual : 90,
//     },
//     {
//       namaProduct : 'Sepatu Warrior Rainbow Tosca Corduroy',
//       hargaSatuan: 120000,
//       kategori : "Sepatu Sneaker",
//       totalTerjual : 90,
//     }
//   ]

//   const hitungTotalPenjualan = (dataPenjualan) => {
//     let n = 0
//     for(let i = 0; i < dataPenjualan.length; i++) {
//         return n += dataPenjualan[i].totalTerjual
//     }
//   }

//   console.log(hitungTotalPenjualan(dataPenjualanPakAldi))

// ============================== END OF NO 7 ============================== //

// 8
// Tugas kamu adalah membuat sebuah function yang berfungsi membantu Ibu
// Daniela untuk mendapatkan informasi berupa Total Keuntungan, Total Modal,
// Produk Buku Terlaris, Penulis Buku Terlaris dan Persentase Keuntungan dari data
// penjualan yang telah disediakan diatas.

    // const dataPenjualanNovel = [
    //     {
    //         idProduct: 'BOOK002421',
    //         namaProduk: 'Pulang - Pergi',
    //         penulis: 'Tere Liye',
    //         hargaBeli: 60000,
    //         hargaJual: 86000,
    //         totalTerjual: 150,
    //         sisaStok: 17,
    //     },
    //     {
    //         idProduct: 'BOOK002351',
    //         namaProduk: 'Selamat Tinggal',
    //         penulis: 'Tere Liye',
    //         hargaBeli: 75000,
    //         hargaJual: 103000,
    //         totalTerjual: 171,
    //         sisaStok: 20,
    //     },
    //     {
    //         idProduct: 'BOOK002941',
    //         namaProduk: 'Garis Waktu',
    //         penulis: 'Fiersa Besari',
    //         hargaBeli: 67000,
    //         hargaJual: 99000,
    //         totalTerjual: 213,
    //         sisaStok: 5,
    //     },
    //     {
    //         idProduct: 'BOOK002941',
    //         namaProduk: 'Laskar Pelangi',
    //         penulis: 'Andrea Hirata',
    //         hargaBeli: 55000,
    //         hargaJual: 68000,
    //         totalTerjual: 20,
    //         sisaStok: 56,
    //     },
    // ];
    
    // function getInfoPenjualan(dataPenjualan) {
    //     let totalKeuntungan = 0;
    //     let totalModal = 0;
    //     let bukuTerlaris = {
    //         namaProduk: '',
    //         totalTerjual: 0,
    //     };
    //     let penulisTerlaris = {
    //         namaPenulis: '',
    //         totalTerjual: 0,
    //     };
        
    //     for (let i = 0; i < dataPenjualan.length; i++) {
    //         totalKeuntungan += (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual;
    //         totalModal += dataPenjualan[i].hargaBeli * dataPenjualan[i].totalTerjual;
            
    //         if (dataPenjualan[i].totalTerjual > bukuTerlaris.totalTerjual) {
    //             bukuTerlaris.namaProduk = dataPenjualan[i].namaProduk;
    //             bukuTerlaris.totalTerjual = dataPenjualan[i].totalTerjual;
    //         }
            
    //         if (dataPenjualan[i].totalTerjual > penulisTerlaris.totalTerjual) {
    //             penulisTerlaris.namaPenulis = dataPenjualan[i].penulis;
    //             penulisTerlaris.totalTerjual = dataPenjualan[i].totalTerjual;
    //         }
    //     }
        
    //     const persentaseKeuntungan = ((totalKeuntungan / totalModal) * 100).toFixed(2);
        
    //     return {
    //         totalKeuntungan: `Rp ${totalKeuntungan.toLocaleString()}`,
    //         totalModal: `Rp ${totalModal.toLocaleString()}`,
    //         persentaseKeuntungan: `${persentaseKeuntungan}%`,
    //         bukuTerlaris: bukuTerlaris.namaProduk,
    //         penulisTerlaris: penulisTerlaris.namaPenulis,
    //     };
    // }
    
    // console.log(getInfoPenjualan(dataPenjualanNovel))

// ============================== END OF NO 8 ============================== //